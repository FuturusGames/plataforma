﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lifes : MonoBehaviour
{

    public static lifes instance;

    public GameObject myPrefab;
    public Sprite fullHeart;
    public Sprite emptyHeart;

    public int health;
    public float numOfHearts;

    private List<GameObject> hearts;

    void Start()
    {
        hearts = new List<GameObject>();

        for (int i = 0; i < (int)numOfHearts; i++)
        {
            CreateHeart(i);
        }

        instance = this;
    }

    private void CreateHeart(int hearthNumber)
    {
        GameObject test = Instantiate(myPrefab, gameObject.transform, false) as GameObject;
        test.transform.localPosition = new Vector3(0.8f * (hearthNumber%5), -0.8f*(int)(hearthNumber/5), 0);
        hearts.Add(test);
    }

    public void IncreaseHearths()
    {
        numOfHearts += 0.25f;
        health = (int)numOfHearts;
        if (numOfHearts % 1 == 0)
        {
            CreateHeart((int)numOfHearts - 1);
        }
    }

    private void Update()
    {
        for (int i = 0; i < (int)numOfHearts; i++)
        {
            if (i < health)
            {
                hearts[i].GetComponent<SpriteRenderer>().sprite = fullHeart;
            }
            else
            {
                hearts[i].GetComponent<SpriteRenderer>().sprite = emptyHeart;
            }
        }
    }
}