﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartAction : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            lifes.instance.IncreaseHearths();
            Destroy(gameObject);
        }
    }
}