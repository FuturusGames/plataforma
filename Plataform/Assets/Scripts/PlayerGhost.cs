﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGhost : MonoBehaviour
{
    private GameObject player;
    private SpriteRenderer sprite;
    public float vanishTime = 0.3f;
    public float alphaMultiplier = 0.85f;
    private float alpha = 0.9f;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        sprite.flipX = true;
        player = GameObject.FindGameObjectWithTag("Player");

        transform.position = player.GetComponentInChildren<SpriteRenderer>().transform.position;
        transform.localScale = player.GetComponentInChildren<SpriteRenderer>().transform.localScale;
        transform.eulerAngles = player.GetComponentInChildren<SpriteRenderer>().transform.eulerAngles;

        sprite.sprite = player.GetComponentInChildren<SpriteRenderer>().sprite;
        

    }

    // Update is called once per frame
    void Update()
    {
        sprite.color = new Vector4(1f, 1f, 1f, alpha);

        alpha *= alphaMultiplier;

        vanishTime -= Time.deltaTime;

        if(vanishTime <= 0)
        {
            Destroy(gameObject);
        }
        
    }
}
