﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spike : MonoBehaviour
{
    private PlayerMovement player;
    public float timeInvulnerable;
    public float timeDamage;

    public Vector2 playerKnockbackSpeed;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Collision!!!");
            
            if (player.isInvulnerable == false)
            {
                lifes.instance.health = lifes.instance.health - 1;
                player.DoDamage(timeInvulnerable, timeDamage);
                StartCoroutine(player.Knockback(0.3f, playerKnockbackSpeed.y, playerKnockbackSpeed.x * (player.transform.position.x - this.transform.position.x)));
            }
            
        }
    }
}
