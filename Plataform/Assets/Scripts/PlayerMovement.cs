﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public float jumpForce;
    public float jumpTime;
    public int extraJumps;
    public float foxDashSpeed;
    public float foxDashTime;
    public float wallSlidingSpeed;
    public float wallJumpingTime;
    public Vector2 wallForce;

    public Transform feetPos;
    public Transform spawnPos;
    public float checkRadius;

    public Transform attackHitBoxPos;
    public float attackRadius, attackDamage;

    public Transform wallCheck;
    public float wallCheckRadius;

    public GameObject[] playerBodyPrefabs;
    public GameObject playerGhost;

    
    public LayerMask enemy;
    public LayerMask ground;
    public LayerMask tree;
    public LayerMask wall;


    public bool isInvulnerable = false;
    private float timeInvulnerableCount;

    public bool takeDamage = false;
    private float timeDamageCount;

    private Rigidbody2D rb;
    private Animator anim;

    private float moveInput;
    private float jumpTimeCounter;
    private float foxDashTimeCount;
    private int extraJumpsCounter;
    

    private bool isGrounded;
    private bool isJumping;
    private bool wallInFront;
    private bool alreadyDashed = false;
    private bool isWallSliding;
    private bool isWallJumping;

    private GameObject[] playerBody = new GameObject[10];

    private int activePlayerPos = -1;
    
    private Vector3[] tableSpawn = { Vector3.zero, 
                                    new Vector3(-0.1f, 0.501f, 0f),
                                    new Vector3(0f, 0.42f, 0f),
                                    Vector3.zero };

    private bool animIsJumping = false;
    private bool animIsRunning = false;
    private bool animIsAttacking = false;


    void Start()
    {
        CreateAllCharacters();
        SetNewCharacter(0);
        rb = GetComponent<Rigidbody2D>();
        extraJumpsCounter = extraJumps;
    }

    private void CreateAllCharacters()
    {
        for (int i = 0; i < playerBodyPrefabs.Length; i++)
        {
            playerBody[i] = Instantiate(playerBodyPrefabs[i], gameObject.transform, false);
            playerBody[i].transform.localPosition = tableSpawn[i];
            playerBody[i].SetActive(false);
        }
    }

    private void SetNewCharacter(int playerPos)
    {
        if (activePlayerPos != playerPos)
        {
            if(activePlayerPos != -1)
            {
                playerBody[activePlayerPos].SetActive(false);
            }
            activePlayerPos = playerPos;
            playerBody[activePlayerPos].SetActive(true);
            anim = GetComponentInChildren<Animator>();
        }
    }

    private void MovementControl()
    {
        moveInput = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        if(Mathf.Abs(moveInput) < Mathf.Epsilon)
        {
            animIsRunning = false;
        }
        else
        {
            animIsRunning = true;
        }

        if (moveInput < 0)
        {
            transform.eulerAngles = new Vector3(0, 0, 0);
        }
        else if (moveInput > 0)
        {
            transform.eulerAngles = new Vector3(0, 180, 0);
        }
    }

    private void JumpControl()
    {
        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, ground | wall);
        wallInFront = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, wall);

        if (isGrounded == true && isJumping == false)
        {
            animIsJumping = false;
            extraJumpsCounter = extraJumps;
            alreadyDashed = false;
        }

        if (isGrounded == true && Input.GetButtonDown("Jump"))
        {
            isJumping = true;
            jumpTimeCounter = jumpTime;
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            animIsJumping = true;
        }
        else if(isJumping == false && Input.GetButtonDown("Jump") && (extraJumpsCounter > 0 || isWallJumping))
        {
            animIsJumping = true;
            extraJumpsCounter--;
            isJumping = true;
            jumpTimeCounter = 2*jumpTime/3;
            rb.velocity = new Vector2(rb.velocity.x, 2*jumpForce/3);
        }
        if(Input.GetButton("Jump") && isJumping == true)
        { 
            if(jumpTimeCounter > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, jumpForce);
                jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                isJumping = false;
            }
        }

        if (Input.GetButtonUp("Jump"))
        {
            isJumping = false;
        }
    }

    IEnumerator CheckAttackHitBox(LayerMask layer, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        Collider2D[] detectedObjects = Physics2D.OverlapCircleAll(attackHitBoxPos.position,attackRadius, layer);
        foreach (Collider2D collider in detectedObjects)
        {
            collider.transform.SendMessage("Damage", attackDamage);
        }
    }

    private void AttackControl()
    {
        switch(activePlayerPos)
        {
            case 0:
            {
                if (Input.GetButtonDown("Special"))
                {
                    if(!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                    {
                        animIsAttacking = true;
                        StartCoroutine(CheckAttackHitBox(enemy, 0.25f));
                    }
                    
                }
                else
                {
                    animIsAttacking = false;
                }
            }
            break;

            case 1:
                {
                    if (Input.GetButtonDown("Special") && alreadyDashed == false)
                    {
                        animIsAttacking = true;
                        foxDashTimeCount = foxDashTime;
                        alreadyDashed = true;
                    }
                    else if (Input.GetButton("Special") && animIsAttacking == true)
                    {
                        if (foxDashTimeCount > 0)
                        {
                            Instantiate(playerGhost, transform.position, Quaternion.identity);
                            rb.velocity += -(Vector2)transform.right*foxDashSpeed;
                            foxDashTimeCount -= Time.deltaTime;
                        }
                        else
                        {
                            animIsAttacking = false;
                            rb.velocity = new Vector2(0f, rb.velocity.y);
                        }
                    }
                    else
                    {
                        animIsAttacking = false;
                    }
                }
                break;

            case 2:
                {
                    if (Input.GetButtonDown("Special"))
                    {
                        animIsAttacking = true;
                        if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
                        {
                            animIsAttacking = true;
                            StartCoroutine(CheckAttackHitBox(tree, 0.25f));
                        }
                    }
                    else
                    {
                        animIsAttacking = false;
                    }
                }
                break;

            case 3:
                {
                    if (wallInFront && !isGrounded && moveInput != 0)
                    {
                        isWallSliding = true;
                        rb.velocity = new Vector2(rb.velocity.x, Mathf.Clamp(rb.velocity.y, -wallSlidingSpeed, float.MaxValue));
                        if (Input.GetButtonDown("Special"))
                        {
                            extraJumpsCounter = extraJumps; 
                            isWallJumping = true;
                            Invoke(nameof(SetWallJumpToFalse), wallJumpingTime);
                        }
                    }
                    else
                    {
                        isWallSliding = false;
                    }

                    if(isWallJumping)
                    {
                        rb.velocity = new Vector2(wallForce.x * -moveInput, wallForce.y);
                    }
                }
                break;

            default:

                break;
        }
    }

    void CharacterControl()
    {
        if (Input.GetButtonDown("Sword"))
        {
            SetNewCharacter(0);
        }
        else if (Input.GetButtonDown("Fox"))
        {
            SetNewCharacter(1);
        }
        else if (Input.GetButtonDown("Bear"))
        {
            SetNewCharacter(2);
        }
        else if(Input.GetButtonDown("Monkey"))
        {
            SetNewCharacter(3);
        }
    }

    void SetWallJumpToFalse()
    {
        isWallJumping = false;
    }

    void AnimationControl()
    {
        anim.SetBool("isRunning", animIsRunning);
        anim.SetBool("isJumping", animIsJumping);
        anim.SetBool("isAttacking", animIsAttacking);
    }

    public IEnumerator Knockback(float knocDur, float knocPwr, float knocDir)
    {
        float timer = 0;
        Debug.Log("Knock!!!");

        while (knocDur > timer)
        {
            timer += Time.deltaTime;
            rb.velocity = Vector2.zero;
            rb.AddForce(new Vector2(knocDir, knocPwr));
        }
        yield return 0;
    }


    public void DoDamage(float timeInvulnerable, float timeDamage)
    {
        timeInvulnerableCount = timeInvulnerable;
        isInvulnerable = true;
        timeDamageCount = timeDamage;
        takeDamage = true;
    }

    public void Respawn()
    {
        transform.position = new Vector3(spawnPos.position.x, spawnPos.position.y, 0f);
    }

    public void DamageControl()
    {
        if (timeInvulnerableCount > 0)
        {
            timeInvulnerableCount -= Time.deltaTime;
        }
        else
        {
            isInvulnerable = false;
        }

        if (timeDamageCount > 0)
        {
            timeDamageCount -= Time.deltaTime;
        }
        else
        {
            takeDamage = false;
        }
    }

    void Update()
    {
        if(takeDamage == false)
        {
            MovementControl();
            JumpControl();
            AttackControl();
            CharacterControl();
        }
        AnimationControl();
        DamageControl();

        if (transform.position.y < -15f)
        {
            Respawn();
            lifes.instance.health = lifes.instance.health - 1;
        }

        if(lifes.instance.health <= 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(attackHitBoxPos.position, attackRadius);
        Gizmos.DrawWireSphere(feetPos.position, checkRadius);
        Gizmos.DrawWireSphere(wallCheck.position, wallCheckRadius);
    }

}
